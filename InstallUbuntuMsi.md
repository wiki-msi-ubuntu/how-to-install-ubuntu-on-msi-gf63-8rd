#INSTALL UBUNTU ON MSI GF63 8RD

1. Prepare iso file and usb for boot ubuntu 18.10 (don't use Ubuntu 18.4 iso file for boot usb,
i don't know why but i have many issues when install this version) 

2. Secure boot disable (change this in BIOS, reboot the system and press delete a couple of times before the OS is loaded)

3. Install Ubuntu "normally"

4. Reboot and try to open a terminal:

Possible problem: Screen freezing while installing/rebooting
- Reboot system
- Go to the Install Ubuntu option (BUT DONT PRESS ENTER)
- Press e
- Add "modprobe.blacklist=nouveau" after "quiet splash"
- Fress F10 to boot

5. After reboot, at grub menu, choose "Ubuntu" but dont press Enter, press "e" to edit grub, 
Add "modprobe.blacklist=nouveau" after "quiet splash"
- Press F10 again

6. when ubuntu displays Login screen, press Alt + Crtl + F2 to go to terminal
run some commands below: 
- sudo apt update
- sudo ubuntu-drivers autoinstall
- sudo udo apt install nvidia-384 (384 is code for gtx 1050 Ti)

DONE! 